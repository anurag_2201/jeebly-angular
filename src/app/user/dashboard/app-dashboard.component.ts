import { Component } from '@angular/core';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';

@Component({
  selector: 'app-dashboard',
  templateUrl: './app-dashboard.component.html'
})
export class AppDashboardComponent {
  title = 'jeebly1';
  user = {};
  constructor(private authService: NbAuthService) {
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {
        console.log(token);
        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
          console.log(this.user);
        }

      });
  }
}
