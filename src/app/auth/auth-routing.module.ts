import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent } from '@nebular/auth';
import { NgxLoginComponent } from './login/login.component';
import { NgxRegisterComponent } from './register/register.component';
import { NgxMainComponent } from './main/main.component';

export const routes: Routes = [
    {
        path: '',
        component: NgxMainComponent,
        children: [
            {
              path: 'login',
              component: NgxLoginComponent, 
            },
            {
              path: 'register',
              component: NgxRegisterComponent, 
            },
          ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class NgxAuthRoutingModule {
}