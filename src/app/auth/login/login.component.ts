import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NbLoginComponent, NbAuthService } from '@nebular/auth';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class NgxLoginComponent extends NbLoginComponent implements OnInit {
  public mCurrent: number = 1;

  ngOnInit() {
    // console.log("ngOnInit:" + this.mCurrent);
    setInterval( () => {
      if (this.mCurrent == 4) {
        this.mCurrent = 1;
      } else {
        this.mCurrent++;
      }
      // console.log("ngOnInit:" + this.mCurrent);
    }, 3000);
  }

  setCurrent(index: any) {
    this.mCurrent = index;
    console.log(this.mCurrent)
  }
}