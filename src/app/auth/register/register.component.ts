import { Component } from '@angular/core';
import { NbRegisterComponent } from '@nebular/auth';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
})
export class NgxRegisterComponent extends NbRegisterComponent {
    public mCurrent: number = 1;

  ngOnInit() {
    // console.log("ngOnInit:" + this.mCurrent);
    setInterval( () => {
      if (this.mCurrent == 4) {
        this.mCurrent = 1;
      } else {
        this.mCurrent++;
      }
      // console.log("ngOnInit:" + this.mCurrent);
    }, 3000);
  }

  setCurrent(index: any) {
    this.mCurrent = index;
    console.log(this.mCurrent)
  }
}