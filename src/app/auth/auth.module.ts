import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthModule } from '@nebular/auth';
import {
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule,
  NbLayoutModule
} from '@nebular/theme';
import { NgxLoginComponent } from './login/login.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgxRegisterComponent } from './register/register.component';
import { NgxMainComponent } from './main/main.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,
    NbEvaIconsModule,
    NbAuthModule,
    NbLayoutModule,
  ],
  declarations: [
    NgxLoginComponent, NgxRegisterComponent, NgxMainComponent
  ],
})
export class NgxAuthModule {
}